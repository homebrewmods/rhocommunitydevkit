using System;
//using SLua;

namespace UnityEngine
{
	//[CustomLuaClass]
	public struct Vector3d
	{
		public Vector3d(double x, double y, double z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public Vector3d(float x, float y, float z)
		{
			this.x = (double)x;
			this.y = (double)y;
			this.z = (double)z;
		}

		public Vector3d(Vector3 v3)
		{
			this.x = (double)v3.x;
			this.y = (double)v3.y;
			this.z = (double)v3.z;
		}

		public Vector3d(double x, double y)
		{
			this.x = x;
			this.y = y;
			this.z = 0.0;
		}

		public double this[int index]
		{
			get
			{
				double result;
				switch (index)
				{
					case 0:
						result = this.x;
						break;
					case 1:
						result = this.y;
						break;
					case 2:
						result = this.z;
						break;
					default:
						throw new IndexOutOfRangeException("Invalid index!");
				}
				return result;
			}
			set
			{
				switch (index)
				{
					case 0:
						this.x = value;
						break;
					case 1:
						this.y = value;
						break;
					case 2:
						this.z = value;
						break;
					default:
						throw new IndexOutOfRangeException("Invalid Vector3d index!");
				}
			}
		}

		public Vector3d normalized
		{
			get
			{
				return Vector3d.Normalize(this);
			}
		}

		public double magnitude
		{
			get
			{
				return Math.Sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
			}
		}

		public double sqrMagnitude
		{
			get
			{
				return this.x * this.x + this.y * this.y + this.z * this.z;
			}
		}

		public static Vector3d zero
		{
			get
			{
				return new Vector3d(0.0, 0.0, 0.0);
			}
		}

		public static Vector3d one
		{
			get
			{
				return new Vector3d(1.0, 1.0, 1.0);
			}
		}

		public static Vector3d forward
		{
			get
			{
				return new Vector3d(0.0, 0.0, 1.0);
			}
		}

		public static Vector3d back
		{
			get
			{
				return new Vector3d(0.0, 0.0, -1.0);
			}
		}

		public static Vector3d up
		{
			get
			{
				return new Vector3d(0.0, 1.0, 0.0);
			}
		}

		public static Vector3d down
		{
			get
			{
				return new Vector3d(0.0, -1.0, 0.0);
			}
		}

		public static Vector3d left
		{
			get
			{
				return new Vector3d(-1.0, 0.0, 0.0);
			}
		}

		public static Vector3d right
		{
			get
			{
				return new Vector3d(1.0, 0.0, 0.0);
			}
		}

		public static Vector3d operator +(Vector3d a, Vector3d b)
		{
			return new Vector3d(a.x + b.x, a.y + b.y, a.z + b.z);
		}

		public static Vector3d operator +(Vector3 a, Vector3d b)
		{
			return new Vector3d((double)a.x + b.x, (double)a.y + b.y, (double)a.z + b.z);
		}

		public static Vector3d operator +(Vector3d a, Vector3 b)
		{
			return new Vector3d(a.x + (double)b.x, a.y + (double)b.y, a.z + (double)b.z);
		}

		public static Vector3d operator -(Vector3d a, Vector3d b)
		{
			return new Vector3d(a.x - b.x, a.y - b.y, a.z - b.z);
		}

		public static Vector3d operator -(Vector3 a, Vector3d b)
		{
			return new Vector3d((double)a.x - b.x, (double)a.y - b.y, (double)a.z - b.z);
		}

		public static Vector3d operator -(Vector3d a, Vector3 b)
		{
			return new Vector3d(a.x - (double)b.x, a.y - (double)b.y, a.z - (double)b.z);
		}

		public static Vector3d operator -(Vector3d a)
		{
			return new Vector3d(-a.x, -a.y, -a.z);
		}

		public static Vector3d operator *(Vector3d a, double d)
		{
			return new Vector3d(a.x * d, a.y * d, a.z * d);
		}

		public static Vector3d operator *(double d, Vector3d a)
		{
			return new Vector3d(a.x * d, a.y * d, a.z * d);
		}

		public static Vector3d operator /(Vector3d a, double d)
		{
			return new Vector3d(a.x / d, a.y / d, a.z / d);
		}

		public static bool operator ==(Vector3d lhs, Vector3d rhs)
		{
			return Vector3d.SqrMagnitude(lhs - rhs) < 0.0;
		}

		public static bool operator !=(Vector3d lhs, Vector3d rhs)
		{
			return Vector3d.SqrMagnitude(lhs - rhs) >= 0.0;
		}

		public static explicit operator Vector3(Vector3d vector3d)
		{
			return new Vector3((float)vector3d.x, (float)vector3d.y, (float)vector3d.z);
		}

		public static explicit operator Vector3d(Vector3 v)
		{
			return new Vector3d(v);
		}

		public static Vector3d Lerp(Vector3d from, Vector3d to, double t)
		{
			t = Mathd.Clamp01(t);
			return new Vector3d(from.x + (to.x - from.x) * t, from.y + (to.y - from.y) * t, from.z + (to.z - from.z) * t);
		}

		public static Vector3d Slerp(Vector3d from, Vector3d to, double t)
		{
			Vector3 v = Vector3.Slerp((Vector3)from, (Vector3)to, (float)t);
			return new Vector3d(v);
		}

		public static void OrthoNormalize(ref Vector3d normal, ref Vector3d tangent)
		{
			Vector3 v = default(Vector3);
			Vector3 v2 = default(Vector3);
			v = (Vector3)normal;
			v2 = (Vector3)tangent;
			Vector3.OrthoNormalize(ref v, ref v2);
			normal = new Vector3d(v);
			tangent = new Vector3d(v2);
		}

		public static void OrthoNormalize(ref Vector3d normal, ref Vector3d tangent, ref Vector3d binormal)
		{
			Vector3 v = default(Vector3);
			Vector3 v2 = default(Vector3);
			Vector3 v3 = default(Vector3);
			v = (Vector3)normal;
			v2 = (Vector3)tangent;
			v3 = (Vector3)binormal;
			Vector3.OrthoNormalize(ref v, ref v2, ref v3);
			normal = new Vector3d(v);
			tangent = new Vector3d(v2);
			binormal = new Vector3d(v3);
		}

		public static Vector3d MoveTowards(Vector3d current, Vector3d target, double maxDistanceDelta)
		{
			Vector3d a = target - current;
			double magnitude = a.magnitude;
			Vector3d result;
			if (magnitude <= maxDistanceDelta || magnitude == 0.0)
			{
				result = target;
			}
			else
			{
				result = current + a / magnitude * maxDistanceDelta;
			}
			return result;
		}

		public static Vector3d RotateTowards(Vector3d current, Vector3d target, double maxRadiansDelta, double maxMagnitudeDelta)
		{
			Vector3 v = Vector3.RotateTowards((Vector3)current, (Vector3)target, (float)maxRadiansDelta, (float)maxMagnitudeDelta);
			return new Vector3d(v);
		}

		public static Vector3d SmoothDamp(Vector3d current, Vector3d target, ref Vector3d currentVelocity, double smoothTime, double maxSpeed)
		{
			double deltaTime = (double)Time.deltaTime;
			return Vector3d.SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
		}

		public static Vector3d SmoothDamp(Vector3d current, Vector3d target, ref Vector3d currentVelocity, double smoothTime)
		{
			double deltaTime = (double)Time.deltaTime;
			double positiveInfinity = double.PositiveInfinity;
			return Vector3d.SmoothDamp(current, target, ref currentVelocity, smoothTime, positiveInfinity, deltaTime);
		}

		public static Vector3d SmoothDamp(Vector3d current, Vector3d target, ref Vector3d currentVelocity, double smoothTime, double maxSpeed, double deltaTime)
		{
			smoothTime = Mathd.Max(0.0001, smoothTime);
			double num = 2.0 / smoothTime;
			double num2 = num * deltaTime;
			double d = 1.0 / (1.0 + num2 + 0.479999989271164 * num2 * num2 + 0.234999999403954 * num2 * num2 * num2);
			Vector3d vector = current - target;
			Vector3d vector3d = target;
			double maxLength = maxSpeed * smoothTime;
			Vector3d vector3d2 = Vector3d.ClampMagnitude(vector, maxLength);
			target = current - vector3d2;
			Vector3d vector3d3 = (currentVelocity + num * vector3d2) * deltaTime;
			currentVelocity = (currentVelocity - num * vector3d3) * d;
			Vector3d vector3d4 = target + (vector3d2 + vector3d3) * d;
			if (Vector3d.Dot(vector3d - current, vector3d4 - vector3d) > 0.0)
			{
				vector3d4 = vector3d;
				currentVelocity = (vector3d4 - vector3d) / deltaTime;
			}
			return vector3d4;
		}

		public void Set(double new_x, double new_y, double new_z)
		{
			this.x = new_x;
			this.y = new_y;
			this.z = new_z;
		}

		public static Vector3d Scale(Vector3d a, Vector3d b)
		{
			return new Vector3d(a.x * b.x, a.y * b.y, a.z * b.z);
		}

		public void Scale(Vector3d scale)
		{
			this.x *= scale.x;
			this.y *= scale.y;
			this.z *= scale.z;
		}

		public static Vector3d Cross(Vector3d lhs, Vector3d rhs)
		{
			return new Vector3d(lhs.y * rhs.z - lhs.z * rhs.y, lhs.z * rhs.x - lhs.x * rhs.z, lhs.x * rhs.y - lhs.y * rhs.x);
		}

		public override int GetHashCode()
		{
			return this.x.GetHashCode() ^ this.y.GetHashCode() << 2 ^ this.z.GetHashCode() >> 2;
		}

		public override bool Equals(object other)
		{
			bool result;
			if (!(other is Vector3d))
			{
				result = false;
			}
			else
			{
				Vector3d vector3d = (Vector3d)other;
				result = (this.x.Equals(vector3d.x) && this.y.Equals(vector3d.y) && this.z.Equals(vector3d.z));
			}
			return result;
		}

		public static Vector3d Reflect(Vector3d inDirection, Vector3d inNormal)
		{
			return -2.0 * Vector3d.Dot(inNormal, inDirection) * inNormal + inDirection;
		}

		public static Vector3d Normalize(Vector3d value)
		{
			double num = Vector3d.Magnitude(value);
			Vector3d result;
			if (num > 9.99999974737875E-06)
			{
				result = value / num;
			}
			else
			{
				result = Vector3d.zero;
			}
			return result;
		}

		public void Normalize()
		{
			double num = Vector3d.Magnitude(this);
			if (num > 9.99999974737875E-06)
			{
				this.x /= num;
				this.y /= num;
				this.z /= num;
			}
			else
			{
				this.x = 0.0;
				this.y = 0.0;
				this.z = 0.0;
			}
		}

		public override string ToString()
		{
			return string.Format("Vector3d({0:F1}, {1:F1}, {2:F1})", new object[]
			{
				this.x,
				this.y,
				this.z
			});
		}

		public string ToString(string format)
		{
			return string.Format("Vector3d({0}, {1}, {2})", new object[]
			{
				this.x.ToString(format),
				this.y.ToString(format),
				this.z.ToString(format)
			});
		}

		public static double Dot(Vector3d lhs, Vector3d rhs)
		{
			return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
		}

		public static Vector3d Project(Vector3d vector, Vector3d onNormal)
		{
			double num = Vector3d.Dot(onNormal, onNormal);
			Vector3d result;
			if (num < 1.40129846432482E-45)
			{
				result = Vector3d.zero;
			}
			else
			{
				result = onNormal * Vector3d.Dot(vector, onNormal) / num;
			}
			return result;
		}

		public static Vector3d Exclude(Vector3d excludeThis, Vector3d fromThat)
		{
			return fromThat - Vector3d.Project(fromThat, excludeThis);
		}

		public static double Angle(Vector3d from, Vector3d to)
		{
			return Mathd.Acos(Mathd.Clamp(Vector3d.Dot(from.normalized, to.normalized), -1.0, 1.0)) * 57.29578;
		}

		public static double Distance(Vector3d a, Vector3d b)
		{
			Vector3d vector3d = new Vector3d(a.x - b.x, a.y - b.y, a.z - b.z);
			return Math.Sqrt(vector3d.x * vector3d.x + vector3d.y * vector3d.y + vector3d.z * vector3d.z);
		}

		public static Vector3d ClampMagnitude(Vector3d vector, double maxLength)
		{
			Vector3d result;
			if (vector.sqrMagnitude > maxLength * maxLength)
			{
				result = vector.normalized * maxLength;
			}
			else
			{
				result = vector;
			}
			return result;
		}

		public static double Magnitude(Vector3d a)
		{
			return Math.Sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
		}

		public static double SqrMagnitude(Vector3d a)
		{
			return a.x * a.x + a.y * a.y + a.z * a.z;
		}

		public static Vector3d Min(Vector3d lhs, Vector3d rhs)
		{
			return new Vector3d(Mathd.Min(lhs.x, rhs.x), Mathd.Min(lhs.y, rhs.y), Mathd.Min(lhs.z, rhs.z));
		}

		public static Vector3d Max(Vector3d lhs, Vector3d rhs)
		{
			return new Vector3d(Mathd.Max(lhs.x, rhs.x), Mathd.Max(lhs.y, rhs.y), Mathd.Max(lhs.z, rhs.z));
		}

		public const float kEpsilon = 1E-05f;

		public double x;

		public double y;

		public double z;
	}
}
