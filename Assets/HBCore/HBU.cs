﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class HBU {

	public static T GetOrAddComponent<T>(this GameObject go) where T : Component
	{
		T t = go.GetComponent<T>();
		if (t == null)
		{
			t = go.gameObject.AddComponent<T>();
		}
		return t;
	}

	public static bool SaveTexture2D(Texture2D t, string path)
	{
		bool result;
		if (t == null)
		{
			result = false;
		}
		else
		{
			try
			{
				byte[] bytes = t.EncodeToPNG();
				File.WriteAllBytes(path, bytes);
			}
			catch
			{
				return false;
			}
			result = true;
		}
		return result;
	}
}
