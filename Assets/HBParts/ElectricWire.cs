using System;
using UnityEngine;
[HBS.SerializeComponentOnlyAttribute]
public class ElectricWire : HBLink {

    public ElectricWireInput NewInput(Property inputProp)
    {
        return new ElectricWireInput();
    }
    public ElectricWireOutput NewOutput(Property inputProp)
    {
        return new ElectricWireOutput();
    }

    public class ElectricWireInput
    {
        public float totalPowerfactor;
        public float totalWatt;
        public void UseWatt(float Wt)
        {

        }
    }
    public class ElectricWireOutput
    {
        public float totalWt;
        public void UseWatt(ElectricWire.ElectricWireInput i, float Wt)
        {

        }
        public void PassWatt(float Wt)
        {

        }
    }
}
