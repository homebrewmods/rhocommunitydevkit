﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[HBS.SerializeComponentOnlyAttribute]
public class FloatWire : HBLink {

    public FloatWireInput NewInput(Property inputProp)
    {
        return new FloatWireInput();
    }

    public FloatWireOutput NewOutput(Property inputProp)
    {
        return new FloatWireOutput();
    }

    public class FloatWireInput
    {
        public float value;
    }

    public class FloatWireOutput
    {
        public float value;

        public void Pass(float value = 0f)
        {

        }
    }
}
