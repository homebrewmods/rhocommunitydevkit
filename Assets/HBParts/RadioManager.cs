﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RhoDevkit.ConditionalEditorAttribute(conditionalClass = "HBTransmitter")]
public static class RadioManager
{
#if HBTransmitter
    public static double earthFrequency;

    public static void Register(HBTransmitter transmitter, int channel)
    {

    }

    public static void UnRegister(HBTransmitter transmitter)
    {

    }

    public static Dictionary<int, List<HBTransmitter>> Radios = new Dictionary<int, List<HBTransmitter>>();
#endif
}
