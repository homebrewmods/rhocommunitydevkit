﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RhoDevkit.ConditionalEditorAttribute(conditionalClass = "HBCCTV")]
public class CCTVManager : MonoBehaviour
{
#if HBCCTV
    public static RenderTextureFormat format = RenderTextureFormat.ARGB32;

    public static int Resolution;

    public static Dictionary<int, List<HBCCTV>> CCTVs = new Dictionary<int, List<HBCCTV>>();

    public static Texture default_img;

    public static bool Enabled = true;

    public static bool ClientSideOnly = true;

    public static bool UnregisterCCTV(int id, HBCCTV cctv)
    {
        return true;
    }

    public static void RegisterCCTV(int id, HBCCTV cctv)
    {

    }
#endif
}
