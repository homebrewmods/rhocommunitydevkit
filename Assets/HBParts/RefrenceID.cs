﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml.Linq;
using System.Xml;
using System.Linq;
using RhoDevkit;

//[ExecuteInEditMode]
[RhoDevkit.PreservePartCode]
[HBS.SerializeAttribute]
public class RefrenceID : MonoBehaviour
{

    public string ID = "";
    public bool instantiatedAtRuntime = false;


}