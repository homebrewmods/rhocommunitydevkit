﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[RhoDevkit.ConditionalEditorAttribute(conditionalClass = "CoG")]
public class CoGEditor
{
#if CoG
    [DrawGizmo(GizmoType.Selected | GizmoType.Active)]
    static void DrawCoGGizmo(CoG cog, GizmoType gizmoType)
    {
        Gizmos.matrix = cog.transform.localToWorldMatrix;
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(cog.cogOffset, 0.05f);
    }
#endif
}
