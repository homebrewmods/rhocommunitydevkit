﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RhoDevkit
{
	public class PreservePartCodeAttribute : Attribute
	{

	}

	public class GeneratedPartCodeAttribute : Attribute
	{

	}
}