﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceholderComponent : MonoBehaviour {
    // placeholder component is used to prevent the serialiser from destroying part indexing and corrupting parts
}
