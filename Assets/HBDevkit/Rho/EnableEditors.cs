﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace RhoDevkit
{
    public static class EnableEditors
    {
        public static List<string> currentConditionals = new List<string>();

        [MenuItem("RhoDevkit/Editor/Enable editor scripts")]
        public static void EnableEditorScripts()
        {
            // for each attribute, enable or disable conditional
            Assembly csassembly = AppDomain.CurrentDomain.GetAssemblies().SingleOrDefault(assembly => assembly.GetName().Name == "Assembly-CSharp");
            var types = from type in csassembly.GetTypes() where Attribute.IsDefined(type, typeof(ConditionalEditorAttribute)) select type;


            foreach (Type type in types)
            {
                var attrs = type.GetCustomAttributes(typeof(ConditionalEditorAttribute), false);

                foreach (ConditionalEditorAttribute attr in attrs)
                {
                    var existingType = Type.GetType(attr.conditionalClass);
                    if (existingType != null)
                    {
                        Debug.Log("enabling " + attr.conditionalClass);
                        AutoDefine.AddDefineIfNecessary(attr.conditionalClass, BuildTargetGroup.Standalone);
                    }
                }
            }
        }

        [MenuItem("RhoDevkit/Editor/Disable editor scripts")]
        public static void DisableEditorScripts()
        {

            // for each attribute, enable or disable conditional
            Assembly csassembly = AppDomain.CurrentDomain.GetAssemblies().SingleOrDefault(assembly => assembly.GetName().Name == "Assembly-CSharp");
            var types = from type in csassembly.GetTypes() where Attribute.IsDefined(type, typeof(ConditionalEditorAttribute)) select type;


            foreach (Type type in types)
            {
                var attrs = type.GetCustomAttributes(typeof(ConditionalEditorAttribute), false);

                foreach (ConditionalEditorAttribute attr in attrs)
                {

                    //currentConditionals.Remove(attr.conditionalClass);
                    Debug.Log("disabling " + attr.conditionalClass);
                    AutoDefine.RemoveDefineIfNecessary(attr.conditionalClass, BuildTargetGroup.Standalone);

                }
            }
        }
    }

    public class ConditionalEditorAttribute : Attribute
    {
        public string conditionalClass;
    }
}