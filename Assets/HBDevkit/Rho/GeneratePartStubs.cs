﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using HBS;
using RhoDevkit;
using System.Linq;


// generate part stubs scans the game assembly file for all part classes with the SerializePart attribute
// from there it checks if its not an excluded (already specially handled) class and generates a new stub part class
// these stub part classes can then be used to generate serialiser schemas

namespace RhoDevkit
{

    public static class GeneratePartStubs
    {
        [MenuItem("RhoDevkit/Serialiser/Generate schema files")]
        public static void GenerateSchemas()
        {
            var partStubDir = new DirectoryInfo(Application.dataPath).FullName + "/HBParts/GeneratedCode";
            if (!Directory.Exists(partStubDir) || Directory.GetFiles(partStubDir).Length == 0)
            {
                //Debug.Log("test");
                // no part stubs?
                //if (EditorUtility.DisplayDialog("No part stub files found", "Generate part stub files now?", "Yes (Strongly Recommended)", "No"))
                //{
                //    GenerateStubs(false);
                //}

                // promting here causes a race condition, temporarily disabled
                if (EditorUtility.DisplayDialog("No part stub files found", "Please generate part stubs before generating schema files", "Damn"))
                {
                    return;
                    //GenerateStubs(false);
                }
            }
            EditorUtility.DisplayProgressBar("Generating schema files", "", 0);
            HBS.CodeGenerator.GenerateCode();
            EditorUtility.ClearProgressBar();
        }

        [MenuItem("RhoDevkit/Serialiser/Generate part stub files")]
        public static void GenerateStubsMenuItem()
        {
            GenerateStubs();
        }

        public static void GenerateStubs(bool genSchemas = true)
        {

            //return;
            string path = UnityEditor.EditorUtility.OpenFilePanel("Open Homebrew Assembly-CSharp.dll", Application.streamingAssetsPath + "/Assets/GameObjects/", "dll");

            if (path == "" || Path.GetFileName(path) != "Assembly-CSharp.dll")
            {
                EditorUtility.DisplayDialog("Wrong file mate", "Please select Homebrew Assembly-CSharp.dll file", "Fine");
                //Debug.Log("Please locate Homebrew Assembly-CSharp.dll file");
                return;
            }

            Debug.Log(path);

            // clean up old files?
            var partStubDir = new DirectoryInfo(Application.dataPath).FullName + "/HBParts/GeneratedCode";

            if (Directory.Exists(partStubDir))
            {
                if (Directory.GetFiles(partStubDir).Length != 0 || Directory.GetDirectories(partStubDir).Length != 0)
                {
                    if (EditorUtility.DisplayDialog("Part stub files found", "Clear existing part stub cache?", "Yes (Recommended)", "No"))
                    {
                        // delete part stub folder contents
                        System.IO.DirectoryInfo di = new DirectoryInfo(partStubDir);

                        foreach (FileInfo file in di.GetFiles())
                        {
                            file.Delete();
                        }
                        foreach (DirectoryInfo dir in di.GetDirectories())
                        {
                            dir.Delete(true);
                        }
                    }
                }
            }
            else
            {
                // create codegen dir
                Directory.CreateDirectory(partStubDir);
                EnableEditors.DisableEditorScripts();
            }

            // clean up schemas
            var partSchemaDir = new DirectoryInfo(Application.dataPath).FullName + "/HBParts/HBWorld/HBS/GeneratedCode";
            if (Directory.GetFiles(partStubDir).Length != 0 || Directory.GetDirectories(partStubDir).Length != 0)
            {
                if (EditorUtility.DisplayDialog("Schema files found", "Clear existing schema cache?", "Yes (Strongly Recommended)", "No"))
                {
                    // delete part schema folder
                    System.IO.DirectoryInfo di = new DirectoryInfo(partSchemaDir);
                    di.Delete(true);

                }
            }

            // progress bar
            EditorUtility.DisplayProgressBar("Generating part stubs", "", 0);

        //}
        //EditorUtility.ClearProgressBar();

            var adal = new SandboxAssemblyLoader();
            var stubGen = new PartStubGenerator();
            adal.LoadAssembly(new FileInfo(path));
            try
            {
                stubGen.HomebrewSerialisedTypes = adal.GetTypes(new List<string>() { "HBS.SerializeAttribute", "HBS.SerializePartAttribute", "HBS.SerializeComponentOnlyAttribute" }, true);
                stubGen.GeneratePartStubFiles();
            }

            finally
            {
                adal.Dispose();
            }
            EditorUtility.ClearProgressBar();

            // promting here causes a race condition, temporarily disabled
            //if (!genSchemas) { return; }
            //if (Directory.GetFiles(partStubDir).Length != 0 || Directory.GetDirectories(partStubDir).Length != 0)
            //{
            //    if (EditorUtility.DisplayDialog("Schema files required", "Generate part schema files now?", "Yes (Recommended)", "No"))
            //    {
            //        EditorUtility.DisplayProgressBar("Generating schema files", "", 0);
            //        HBS.CodeGenerator.GenerateCode();
            //        EditorUtility.ClearProgressBar();

            //    }
            //}

            if (Directory.GetFiles(partStubDir).Length == 0 || Directory.GetDirectories(partStubDir).Length == 0)
            {
                if (EditorUtility.DisplayDialog("Schema files required", "Please generate schema files now", "Okay then"))
                {
                    return;
                    //EditorUtility.DisplayProgressBar("Generating schema files", "", 0);
                    //HBS.CodeGenerator.GenerateCode();
                    //EditorUtility.ClearProgressBar();

                }
            }

        }

    }

    public class PartStubGenerator
    {
        public List<SandboxAssemblyLoader.MarshalType> HomebrewSerialisedTypes;

        public void GeneratePartStubFiles()
        {
            if (HomebrewSerialisedTypes.Count < 1) { return; }
            var dir = new DirectoryInfo(Application.dataPath).FullName + "/HBParts/GeneratedCode";
            var perFile = 1f / HomebrewSerialisedTypes.Count;
            int i = 0;
            foreach (SandboxAssemblyLoader.MarshalType mType in HomebrewSerialisedTypes)
            {
                i++;
                EditorUtility.DisplayProgressBar("Generating part stubs", "Generating " + mType.Name + "...", (float)(perFile * i));
                GeneratePartFile(dir, mType);
            }

        }

        public void GeneratePartFile(String dir, SandboxAssemblyLoader.MarshalType type)
        {

            // check if this type exists, and if it does, should we overwrite it?
            var existingType = Type.GetType(type.Name);
            if (existingType != null)
            {
                if (!existingType.IsDefined(typeof(RhoDevkit.GeneratedPartCodeAttribute), true))
                {
                    return;
                }

            }

            var fileStr = FormatClass(type, "");
            if (fileStr.Length < 1) { return; }
            using (var script = File.CreateText(dir + "/" + type.Name + ".cs"))
            {
                script.Write(fileStr);
            }

        }

        private List<string> blacklistNamespaces = new List<string>()
        {
            "System",
            "UnityEngine",
        };


        private string FormatClass(SandboxAssemblyLoader.MarshalType type, string indent)
        {
            #region dont_open_this
            // oh god what have you done

            var fileStr = "";
            if (type.Attributes == null) { return ""; }

            // special type handling
            if (type.BaseType == "enum")
            {
                // enum
                if (!type.IsNested)
                {
                    fileStr += indent + "using System;\n";
                    fileStr += indent + "using UnityEngine;\n";

                }
                if (type.Namespace != null)
                {
                    fileStr += indent + "namespace " + type.Namespace + "\n";
                    fileStr += indent + "{\n";
                    indent += "    ";
                }
                if (!type.IsNested)
                {
                    fileStr += indent + "[RhoDevkit.GeneratedPartCodeAttribute]\n";
                }
                // iterate attributes and apply

                foreach (string attrib in type.Attributes)
                {
                    if (attrib == "HBS.SerializeComponentOnlyAttribute")
                    {
                        fileStr += indent + "[HBS.SerializeComponentOnlyAttribute]\n";
                        continue;
                    }
                    else if (attrib == "HBS.SerializePartAttribute")
                    {
                        fileStr += indent + "[HBS.SerializePartAttribute]\n";
                        continue;
                    }
                    else if (attrib == "HBS.SerializeAttribute")
                    {
                        fileStr += indent + "[HBS.SerializeAttribute]\n";
                        continue;
                    }
                }

                fileStr += indent + "public enum " + type.Name + "\n";
                fileStr += indent + "{\n";
                indent += "    ";

                foreach (SandboxAssemblyLoader.MarshalFieldInfo field in type.FieldInfo)
                {
                    fileStr += indent + field.Name + " = " + field.Value;
                    if (type.FieldInfo.Last() == field)
                    {
                        fileStr += "\n";
                    }
                    else
                    {
                        fileStr += ",\n";
                    }
                    //Debug.Log(field.Name);
                    //Debug.Log(field.Value);
                }
                indent = indent.Remove(indent.Length - 4, 4);
                fileStr += indent + "}\n";
                return fileStr;
            }
            else if (type.BaseType == "struct")
            {
                // struct
                // SerializeComponentOnly
                if (!type.IsNested)
                {
                    fileStr += indent + "using System;\n";
                    fileStr += indent + "using UnityEngine;\n";

                }
                if (type.Namespace != null)
                {
                    fileStr += indent + "namespace " + type.Namespace + "\n";
                    fileStr += indent + "{\n";
                    indent += "    ";
                }
                if (!type.IsNested)
                {
                    fileStr += indent + "[RhoDevkit.GeneratedPartCodeAttribute]\n";
                }
                // iterate attributes and apply

                foreach (string attrib in type.Attributes)
                {
                    if (attrib == "HBS.SerializeComponentOnlyAttribute")
                    {
                        fileStr += indent + "[HBS.SerializeComponentOnlyAttribute]\n";
                        continue;
                    } else if (attrib == "HBS.SerializePartAttribute")
                    {
                        fileStr += indent + "[HBS.SerializePartAttribute]\n";
                        continue;
                    } else if (attrib == "HBS.SerializeAttribute")
                    {
                        fileStr += indent + "[HBS.SerializeAttribute]\n";
                        continue;
                    }
                }

                fileStr += indent + "public struct " + type.Name + "\n";
                fileStr += indent + "{\n";
                indent += "    ";


                foreach (SandboxAssemblyLoader.MarshalFieldInfo field in type.FieldInfo)
                {
                    // how much should we serialise?
                    if (!type.Attributes.Contains("HBS.SerializeAttribute"))
                    {
                        if (field.Attributes == null) { continue; }
                        if (!field.IsPublic) { continue; }
                        if (!field.Attributes.Contains("HBS.SerializePartVarAttribute")) { continue; }
                    }

                    var fiType = field.FieldType;
                    if (field.IsArray) { fiType += "[]"; }
                    if (field.Value != null)
                    {
                        // field has default value
                        fileStr += indent + "public " + fiType + " " + field.Name + " = " + field.Value + ";\n";
                    }
                    else
                    {
                        // no default value
                        fileStr += indent + "public " + fiType + " " + field.Name + ";\n";
                    }
                }

                if (type.NestedTypes != null)
                {
                    foreach (SandboxAssemblyLoader.MarshalType nestedType in type.NestedTypes)
                    {
                        fileStr += FormatClass(nestedType, indent);
                    }
                }

                indent = indent.Remove(indent.Length - 4, 4);
                fileStr += indent + "}\n";

                if (type.Namespace != null)
                {
                    indent = indent.Remove(indent.Length - 4, 4);
                    fileStr += indent + "}\n";
                }

                return fileStr;
            }

            // check attributes, build accordingly
            if (type.Attributes.Contains("HBS.SerializeComponentOnlyAttribute"))
            {
                // SerializeComponentOnly
                if (!type.IsNested)
                {
                    fileStr += indent + "using System;\n";
                    fileStr += indent + "using UnityEngine;\n";

                }
                if (type.Namespace != null)
                {
                    fileStr += indent + "namespace " + type.Namespace + "\n";
                    fileStr += indent + "{\n";
                    indent += "    ";
                }
                if (!type.IsNested)
                {
                    fileStr += indent + "[HBS.SerializeComponentOnlyAttribute]\n";
                    fileStr += indent + "[RhoDevkit.GeneratedPartCodeAttribute]\n";
                }
                fileStr += indent + "public class " + type.Name + " : " + type.BaseType + " {\n";

                fileStr += indent + "}\n";

                if (type.Namespace != null)
                {
                    indent = indent.Remove(indent.Length - 4, 4);
                    fileStr += indent + "}\n";
                }

                return fileStr;
            }
            else if (type.Attributes.Contains("HBS.SerializePartAttribute"))
            {
                // generally used to mean this is an actual physics part class
                if (!type.IsNested)
                {
                    fileStr += indent + "using System;\n";
                    fileStr += indent + "using UnityEngine;\n";

                    // dynamically add namespaces if needed
                    foreach (SandboxAssemblyLoader.MarshalFieldInfo field in type.FieldInfo)
                    {
                        if (field.Attributes == null) { continue; }
                        if (!field.IsPublic) { continue; }
                        if (!field.Attributes.Contains("HBS.SerializePartVarAttribute")) { continue; }

                        if (field.Namespace != null)
                        {
                            if (blacklistNamespaces.Contains(field.Namespace)) { continue; }
                            if (field.Namespace != type.Namespace)
                            {
                                fileStr += indent + "using " + field.Namespace + ";\n";
                            }
                        }
                    }

                }
                if (type.Namespace != null)
                {
                    fileStr += indent + "namespace " + type.Namespace + "\n";
                    fileStr += indent + "{\n";
                }
                if (!type.IsNested)
                {
                    fileStr += indent + "[HBS.SerializePartAttribute]\n";
                    fileStr += indent + "[RhoDevkit.GeneratedPartCodeAttribute]\n";
                    fileStr += indent + "public class " + type.Name + " : " + type.BaseType + " {\n";
                }
                else
                {
                    fileStr += indent + "[HBS.SerializePartAttribute]\n";
                    fileStr += indent + "public class " + type.Name + "\n";
                    fileStr += indent + "{\n";
                }
                indent += "    ";
                // iterate all fields, format and write

                foreach (SandboxAssemblyLoader.MarshalFieldInfo field in type.FieldInfo)
                {
                    if (field.Attributes == null) { continue; }
                    if (!field.IsPublic) { continue; }
                    if (!field.Attributes.Contains("HBS.SerializePartVarAttribute")) { continue; }

                    var fiType = field.FieldType;
                    if (field.IsArray) { fiType += "[]"; }
                    //if (field.Value != null)
                    //{
                    //    // field has default value
                    //    fileStr += indent + "    public " + fiType + " " + field.Name + " = " + field.Value + ";\n";
                    //}
                    //else
                    //{
                    // no default value
                    fileStr += indent + "[HBS.SerializePartVarAttribute]\n";
                    fileStr += indent + "public " + fiType + " " + field.Name + ";\n";
                    //}
                }
                if (type.NestedTypes != null)
                {
                    foreach (SandboxAssemblyLoader.MarshalType nestedType in type.NestedTypes)
                    {
                        fileStr += FormatClass(nestedType, indent);
                    }
                }
                indent = indent.Remove(indent.Length - 4, 4);
                fileStr += indent + "}\n";

                if (type.Namespace != null)
                {
                    indent = indent.Remove(indent.Length - 4, 4);
                    fileStr += indent + "}\n";
                }
                return fileStr;
            }
            else if (type.Attributes.Contains("HBS.SerializeAttribute"))
            {
                //Debug.Log(type.Name);
                // these are odd? just serialise everything, it seems
                if (!type.IsNested)
                {
                    fileStr += indent + "using System;\n";
                    fileStr += indent + "using UnityEngine;\n";
                    // dynamically add namespaces if needed
                    foreach (SandboxAssemblyLoader.MarshalFieldInfo field in type.FieldInfo)
                    {
                        if (field.Namespace != null)
                        {
                            if (blacklistNamespaces.Contains(field.Namespace)) { continue; }
                            if (field.Namespace != type.Namespace)
                            {
                                fileStr += indent + "using " + field.Namespace + ";\n";
                            }
                        }
                    }
                }
                if (type.Namespace != null)
                {
                    fileStr += indent + "namespace " + type.Namespace + "\n";
                    fileStr += indent + "{\n";
                    indent += "    ";
                }
                fileStr += indent + "[HBS.SerializeAttribute]\n";
                if (!type.IsNested)
                {
                    fileStr += indent + "[RhoDevkit.GeneratedPartCodeAttribute]\n";
                    fileStr += indent + "public class " + type.Name + " : " + type.BaseType + " {\n";
                }
                else
                {
                    fileStr += indent + "public class " + type.Name + "\n";
                    fileStr += indent + "{\n";
                }


                indent += "    ";

                foreach (SandboxAssemblyLoader.MarshalFieldInfo field in type.FieldInfo)
                {
                    if (!field.IsPublic) { continue; }
                    var fiType = field.FieldType;
                    if (field.IsArray) { fiType += "[]"; }
                    if (field.Value != null)
                    {
                        // field has default value
                        fileStr += indent + "public " + fiType + " " + field.Name + " = " + field.Value + ";\n";
                    }
                    else
                    {
                        // no default value
                        fileStr += indent + "public " + fiType + " " + field.Name + ";\n";
                    }
                }
                if (type.NestedTypes != null)
                {
                    foreach (SandboxAssemblyLoader.MarshalType nestedType in type.NestedTypes)
                    {
                        fileStr += FormatClass(nestedType, indent);
                    }
                }

                indent = indent.Remove(indent.Length - 4, 4);

                fileStr += indent + "}\n";

                if (type.Namespace != null)
                {
                    indent.Remove(indent.Length - 4, 4);
                    fileStr += indent + "}\n";
                }
                return fileStr;
            }

            return fileStr;

            #endregion
        }
    }
}
