﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace RhoDevkit
{
    public class HomebrewLink
	{
        [SerializeField]
        public static string homebrewDataPath = "";

        [MenuItem("RhoDevkit/Homebrew/Link to homebrew")]
        public static void LinkToHomebrewDirectory()
        {
            string path = UnityEditor.EditorUtility.OpenFolderPanel("Locate Homebrew Data directory", homebrewDataPath, "HB_Data");
            if (path == "") { return; }
            //Debug.Log(path);
            homebrewDataPath = path;
            //GameObject o = HBS.AssetManager.Instantiate(path);
            //o.transform.position = Vector3.zero;
            //o.transform.rotation = Quaternion.identity;
            //Debug.Log(homebrewDataPath);
        }
    }
}