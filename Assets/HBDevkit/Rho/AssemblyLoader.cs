﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;
using System.Security.Policy;
using System.Reflection;
using System.Diagnostics.CodeAnalysis;
using HBS;
using UnityEngine;

namespace RhoDevkit
{

    public class SandboxAssemblyLoader
    {
        public static string GetFriendlyName(Type type)
        {
            if (type == typeof(int))
                return "int";
            else if (type == typeof(short))
                return "short";
            else if (type == typeof(byte))
                return "byte";
            else if (type == typeof(bool))
                return "bool";
            else if (type == typeof(long))
                return "long";
            else if (type == typeof(float))
                return "float";
            else if (type == typeof(double))
                return "double";
            else if (type == typeof(decimal))
                return "decimal";
            else if (type == typeof(string))
                return "string";
            else if (type.IsGenericType)
                return type.Name.Split('`')[0] + "<" + string.Join(", ", type.GetGenericArguments().Select(x => GetFriendlyName(x)).ToArray()) + ">";
            else
                return type.Name;
        }

        public static string GetFriendlyClass(Type type)
        {
            if (type.ToString() == "UnityEngine.MonoBehaviour")
                return "MonoBehaviour";
            else if (type.ToString() == "System.Enum")
                return "enum";
            else if (type.ToString() == "System.Object")
                return "object";
            else if (type.ToString() == "System.ValueType")
                return "struct";
            else
                return type.ToString();
        }

        Type loaderType = typeof(AssemblyLoader);
        AppDomain childDomain;
        AssemblyLoader loader;

        #region Public Methods

        public void LoadAssembly(FileInfo assemblyLocation)
        {


            if (string.IsNullOrEmpty(assemblyLocation.Directory.FullName))
            {
                throw new InvalidOperationException(
                    "Directory can't be null or empty.");
            }

            if (!Directory.Exists(assemblyLocation.Directory.FullName))
            {
                throw new InvalidOperationException(
                   string.Format(CultureInfo.CurrentCulture,
                   "Directory not found {0}",
                   assemblyLocation.Directory.FullName));
            }

            childDomain = BuildChildDomain(AppDomain.CurrentDomain);


            try
            {
                //Type loaderType = typeof(AssemblyLoader);
                if (loaderType.Assembly != null)
                {
                    loader = (AssemblyLoader)childDomain.CreateInstanceFrom(loaderType.Assembly.Location, loaderType.FullName).Unwrap();

                    loader.LoadAssembly(assemblyLocation.FullName);
                    loader.LoadEventHandler(assemblyLocation.Directory.FullName);


                }
                //return types;
            }

            finally
            {
                //AppDomain.Unload(childDomain);
            }
        }

        public List<MarshalType> GetTypes(List<string> attributes, bool inherit)
        {
            List<MarshalType> types = loader.GetTypes(attributes, inherit);
            //types = loader.GetTypes( attributes, inherit );
            return types;
        }

        public List<MarshalFieldInfo> GetTypeFields(MarshalType type)
        {
            List<MarshalFieldInfo> mfi = loader.GetTypeFields(type);
            //mfi = loader.GetTypeFields(type);
            return mfi;
        }

        // Dispose must be called after completion
        public void Dispose()
        {
            loader.Dispose();
            AppDomain.Unload(childDomain);
        }
        #endregion

        #region Private Methods
        private AppDomain BuildChildDomain(AppDomain parentDomain)
        {
            Evidence evidence = new Evidence(parentDomain.Evidence);
            AppDomainSetup setup = parentDomain.SetupInformation;
            return AppDomain.CreateDomain("DiscoveryRegion",
                evidence, setup);
        }
        #endregion

        public class MarshalType : MarshalByRefObject
        {
            public string Name;
            public string BaseType;
            public string Namespace;
            public string FullName;
            public bool IsEnum;
            public List<string> Attributes;
            public List<MarshalFieldInfo> FieldInfo;
            public bool IsNested;
            public List<MarshalType> NestedTypes;
        }

        public class MarshalFieldInfo: MarshalByRefObject
        {
            //public FieldInfo fi;
            public string Name;
            public string FieldType;
            public string Namespace;
            public string Value;
            public bool IsArray = false;
            public bool IsPublic;
            public List<string> Attributes;

        }


        class AssemblyLoader : MarshalByRefObject
        {

            Assembly reflectionOnlyAssembly;
            ResolveEventHandler resolveEventHandler;

            #region Private/Internal Methods

            internal MarshalType ToMarshalType(Type type)
            {
                var mType = new MarshalType();
                mType.Name = type.Name;
                mType.BaseType = GetFriendlyClass(type.BaseType);
                mType.Namespace = type.Namespace;
                mType.FullName = type.FullName;
                mType.IsEnum = type.IsEnum;

                foreach (Attribute attrib in type.GetCustomAttributes(true))
                {
                    if (mType.Attributes == null) { mType.Attributes = new List<String>(); };
                    mType.Attributes.Add(attrib.ToString());
                }
                mType.IsNested = type.IsNested;

                if (mType.IsEnum)
                {
                    //var enumValues = Enum.GetValues(type);
                    foreach (int enumValue in Enum.GetValues(type))
                    {
                        if (mType.FieldInfo == null) { mType.FieldInfo = new List<MarshalFieldInfo>(); };
                        var enumField = new MarshalFieldInfo();
                        enumField.Name = Enum.GetName(type, enumValue);
                        enumField.Value = enumValue.ToString();
                        mType.FieldInfo.Add(enumField);
                    }
                }
                else
                {
                    mType.FieldInfo = GetTypeFields(type);

                    foreach (Type nested in type.GetNestedTypes())
                    {
                        if (mType.NestedTypes == null) { mType.NestedTypes = new List<MarshalType>(); };
                        MarshalType nestedmType = ToMarshalType(nested);
                        mType.NestedTypes.Add(nestedmType);
                    }
                }
                return mType;
            }

            internal List<MarshalType> GetTypes(List<string> attributes, bool inherit)
            {
                List<MarshalType> types = new List<MarshalType>();

                var _attributes = new List<Type>();
                foreach (string attrib in attributes)
                {

                    _attributes.Add(reflectionOnlyAssembly.GetType(attrib));
                }

                foreach (var type in reflectionOnlyAssembly.GetTypes())
                {

                    foreach (Type attrib in _attributes)
                    {
                        if (type.IsDefined(attrib, inherit))
                        {
                            if (!type.IsNested)
                            {
                                var mType = ToMarshalType(type);
                                //mType.Name = type.Name;
                                //mType.BaseType = type.BaseType.ToString();
                                //mType.Namespace = type.Namespace;
                                //mType.FullName = type.FullName;
                                //mType.IsNested = type.IsNested;
                                //foreach (Type nested in type.GetNestedTypes())
                                //{
                                //    var nestedMType = new MarshalType();
                                //    mType.NestedTypes.Add()
                                //}
                                types.Add(mType);
                            }
                        }
                    }

                }


                return types;
            }

            internal List<MarshalFieldInfo> GetTypeFields(MarshalType type)
            {
                var typeClass = reflectionOnlyAssembly.GetType(type.Name);
                if (typeClass == null)
                {
                    typeClass = reflectionOnlyAssembly.GetType(type.FullName);
                }
                if (typeClass == null) 
                {
                    //Debug.Log(type.Namespace + "   " + type.Name + " was null");
                    return null;
                };
                return GetTypeFields(typeClass);
            }

            internal List<MarshalFieldInfo> GetTypeFields(Type type)
            {

                List<MarshalFieldInfo> _fi = new List<MarshalFieldInfo>();
                var fi = type.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);
                foreach (FieldInfo info in fi)
                {
                    
                    var mfi = new MarshalFieldInfo();
                    mfi.Name = info.Name;
                    if (info.FieldType.IsArray)
                    {
                        mfi.FieldType = GetFriendlyName(info.FieldType.GetElementType());
                        mfi.IsArray = true;
                    }
                    else
                    {
                        mfi.FieldType = GetFriendlyName(info.FieldType);
                    }
                    mfi.Namespace = info.FieldType.Namespace;
                    //if (info.GetValue(null) != null) { mfi.Value = info.GetValue(null).ToString(); }
                    mfi.IsPublic = info.IsPublic;
                    foreach (Attribute attrib in info.GetCustomAttributes(true))
                    {
                        //Debug.Log(attrib);
                        if (mfi.Attributes == null) { mfi.Attributes = new List<String>(); };
                        mfi.Attributes.Add(attrib.ToString());
                    }
                    _fi.Add(mfi);
                }

                var pi = type.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo info in pi)
                {

                    var mfi = new MarshalFieldInfo();
                    mfi.Name = info.Name;
                    if (info.PropertyType.IsArray)
                    {
                        mfi.FieldType = GetFriendlyName(info.PropertyType.GetElementType());
                        mfi.IsArray = true;
                    }
                    else
                    {
                        mfi.FieldType = GetFriendlyName(info.PropertyType);
                    }
                    mfi.Namespace = info.PropertyType.Namespace;
                    //if (info.GetValue(null) != null) { mfi.Value = info.GetValue(null).ToString(); }
                    //if (info.CanWrite && info.GetSetMethod(/*nonPublic*/ true).IsPublic)
                    //{
                        mfi.IsPublic = info.CanWrite && info.GetSetMethod(/*nonPublic*/ true).IsPublic;
                    //}

                    //mfi.IsPublic = info.;
                    foreach (Attribute attrib in info.GetCustomAttributes(true))
                    {
                        //Debug.Log(attrib);
                        if (mfi.Attributes == null) { mfi.Attributes = new List<String>(); };
                        mfi.Attributes.Add(attrib.ToString());
                    }
                    _fi.Add(mfi);
                }

                return _fi;
            }

            private Assembly OnReflectionOnlyResolve(
                ResolveEventArgs args, DirectoryInfo directory)
            {
                Assembly loadedAssembly =
                    AppDomain.CurrentDomain.ReflectionOnlyGetAssemblies()
                        .FirstOrDefault(
                          asm => string.Equals(asm.FullName, args.Name,
                              StringComparison.OrdinalIgnoreCase));

                if (loadedAssembly != null)
                {
                    return loadedAssembly;
                }

                AssemblyName assemblyName =
                    new AssemblyName(args.Name);
                string dependentAssemblyFilename =
                    Path.Combine(directory.FullName,
                    assemblyName.Name + ".dll");

                if (File.Exists(dependentAssemblyFilename))
                {
                    return Assembly.ReflectionOnlyLoadFrom(
                        dependentAssemblyFilename);
                }
                return Assembly.ReflectionOnlyLoad(args.Name);
            }



            [SuppressMessage("Microsoft.Performance",
                "CA1822:MarkMembersAsStatic")]
            internal void LoadAssembly(String assemblyPath)
            {

                //DirectoryInfo directory = new DirectoryInfo(assemblyPath);
                //resolveEventHandler = (s, e) => { return OnReflectionOnlyResolve(e, directory); };

                //AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve += resolveEventHandler;
                try
                {
                    Assembly.ReflectionOnlyLoadFrom(assemblyPath);
                }
                catch (FileNotFoundException)
                {
                    /* Continue loading assemblies even if an assembly
                     * can not be loaded in the new AppDomain. */
                }

                reflectionOnlyAssembly = AppDomain.CurrentDomain.ReflectionOnlyGetAssemblies().First();
            }

            internal void LoadEventHandler(String path)
            {
                DirectoryInfo directory = new DirectoryInfo(path);
                resolveEventHandler = (s, e) => { return OnReflectionOnlyResolve(e, directory); };

                AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve += resolveEventHandler;
            }

            internal void Dispose()
            {
                AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve -= resolveEventHandler;
            }
                #endregion
            }
    }
}